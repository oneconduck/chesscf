@extends('layouts._adminlayout')

@section('content')
<!-- BEGIN: Subheader -->
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title">Cấu hình</h3>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-md-12">
            <div class="m-portlet m-portlet--mobile">
                <form method="POST" action='{{ route("save_config.admin") }}' class="m-form form-ajax">
                    <div class="m-portlet__body">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#" data-target="#m_tabs_1_1">Danh mục
                                    chi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#m_tabs_1_2">Danh mục thu</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="m_tabs_1_1" role="tabpanel">
                                <div class="form-group m-form__group">
                                    <label for="exampleInputPassword1">Cấu hình danh mục chi</label>
                                    <div class="form-group">
                                        <input type="text" data-role="tagsinput" class="form-control m-input"
                                            id="expense" name="expense" placeholder="Ấn enter để lưu">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="m_tabs_1_2" role="tabpanel">
                                <div class="form-group m-form__group">
                                    <label for="exampleInputPassword1">Cấu hình danh mục thu</label>
                                    <div class="form-group">
                                        <input type="text" data-role="tagsinput" class="form-control m-input"
                                            id="income" name="income" placeholder="Ấn enter để lưu">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="Create group" aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm/sửa nhóm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="m-form m-form--fit m-form--label-align-right">
                    <input type="hidden" id="group_id">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="group_name">Tên nhóm</label>
                            <input type="text" class="form-control m-input" name="group_name" id="group_name"
                                aria-describedby="emailHelp" placeholder="Nhập vào tên nhóm người dùng">
                            <span class="error_msg"></span>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="group_code">Mã nhóm</label>
                            <input type="text" class="form-control m-input" name="group_code" id="group_code"
                                placeholder="Mã nhóm người dùng">
                            <span class="error_msg"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="save_group" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        var elt = $('#expense');
        elt.tagsinput({
        itemValue: 'value',
        itemText: 'text'
        });
        elt.tagsinput('add', { "value": 1 , "text": "Amsterdam"   , "continent": "Europe"    });
        elt.tagsinput('add', { "value": 4 , "text": "Washington"  , "continent": "America"   });
        elt.tagsinput('add', { "value": 7 , "text": "Sydney"      , "continent": "Australia" });
        elt.tagsinput('add', { "value": 10, "text": "Beijing"     , "continent": "Asia"      });
        elt.tagsinput('add', { "value": 13, "text": "Cairo"       , "continent": "Africa"    });
    })
        
</script>
<style>
    .dataTables_scroll::-webkit-scrollbar {
        width: 6px;
        background-color: #F5F5F5;
    }

    .label {
        display: inline;
        padding: .2em .6em .3em;
        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;
    }

    .label-info {
        background-color: #5bc0de;
    }

    .bootstrap-tagsinput {
        width: 100%;
    }
</style>
@endsection